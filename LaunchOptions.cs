﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Web;

namespace applaunchProtocolHandler
{
	[AttributeUsage(AttributeTargets.Property)]
	public class LaunchOptionNameAttribute : Attribute
	{
		public string ArgName { get; protected set; }
		public LaunchOptionNameAttribute(string Arg) { ArgName = Arg; }
	}

	public class LaunchOptions
	{
		public LaunchOptions() { }

		[LaunchOptionName("")]
		[Description("Path to the application to launch")]
		[DefaultValue("")]
		public string LaunchURL { get; set; }

		[LaunchOptionName("args")]
		[Description("Command line options to pass to launched app")]
		[DefaultValue("")]
		public string CmdLineArgs { get; set; }

		[LaunchOptionName("depth")]
		[Description("Number of subdirectories deep to recurse")]
		[DefaultValue(int.MaxValue)]
		public int RecurseDepth { get; set; }

		[LaunchOptionName("ignore")]
		[Description("Regular expression of items to ignore")]
		[DefaultValue("^$")]
		public string IgnoreRegex { get; set; }

		[LaunchOptionName("match")]
		[Description("Regular expression matching all items to be included")]
		[DefaultValue(".*")]
		public string MatchRegex { get; set; }

		[LaunchOptionName("norun")]
		[Description("Don't launch, just copy to cache")]
		[DefaultValue(false)]
		public bool SkipLaunch { get; set; }

		[LaunchOptionName("nocopy")]
		[Description("Skip copying, just run cached version")]
		[DefaultValue(false)]
		public bool SkipCopying { get; set; }

		[LaunchOptionName("force")]
		[Description("Ignore cached file sizes and modtimes; always copy all files to cache")]
		[DefaultValue(false)]
		public bool ForceCacheUpdate { get; set; }

		public override string ToString()
		{
			IDictionary OutArgs = new HybridDictionary(true);
			foreach ( PropertyInfo PI in GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public) )
				if ( PI.CanRead && (PI.GetIndexParameters().Length == 0) )
				{
					object DefaultVal = null;
					foreach ( DefaultValueAttribute DV in PI.GetCustomAttributes(
						typeof(DefaultValueAttribute), true) )
						if ( DV.Value.GetType() == PI.PropertyType )
							DefaultVal = DV.Value;
					foreach ( LaunchOptionNameAttribute LONA in PI.GetCustomAttributes(
						typeof(LaunchOptionNameAttribute), true) )
					{
						object O = PI.GetValue(this, null);
						if ( O.ToString() != DefaultVal.ToString() )
							OutArgs[LONA.ArgName.Trim().ToLower()] = O;
					}
				}
			ArrayList Params = new ArrayList();
			if ( OutArgs.Contains(string.Empty) )
			{
				object O = OutArgs[string.Empty];
				if ( O is string )
					Params.Add(O.ToString());
				OutArgs.Remove(string.Empty);
			}
			string[] ArgList = new string[OutArgs.Count];
			OutArgs.Keys.CopyTo(ArgList, 0);
			Array.Sort(ArgList, new CaseInsensitiveComparer());
			foreach ( string Arg in ArgList )
			{
				object O = OutArgs[Arg];
				if ( O is bool )
				{
					if ( (bool)O )
						Params.Add(HttpUtility.UrlEncode(Arg));
				}
				else
					Params.Add(HttpUtility.UrlEncode(Arg) + "="
						+ HttpUtility.UrlEncode(O.ToString()));
			}
			return "applaunch:" + string.Join("&", (string[])Params.ToArray(typeof(string)));
		}

		#region Static Method ProcessArguments
		/// <summary>
		/// This method takes all of the command line options and
		/// converts them to a dictionary.  It handles both traditional
		/// command line arguments, and those parsed from an applaunch:
		/// URL (in URL form, the first part is treated as "bare",
		/// while the others are treated as "-option" args).  The
		/// "bare" argument (containing the path to the object to launch)
		/// will be stored in the dictionary's string.Empty entry.
		/// </summary>
		private static IDictionary ProcessArguments(string[] Args)
		{
			// Create a new case-insensitive dictionary to hold
			// all command line options.
			IDictionary ArgDict = new HybridDictionary(true);

			// Walk through and parse each argument...
			foreach ( string Arg in Args )
			{
				// Arguments starting with a - or a / are "traditional"
				// command line arguments.
				if ( Arg.StartsWith("-") || Arg.StartsWith("/") )
				{
					// Strip the -'s and /'s off the argument.  Some may
					// be in the form of /opt=val, in which case, we need
					// to split the argument on ='s.
					string A = Arg;
					while ( A.StartsWith("-") || A.StartsWith("/") )
						A = Arg.Substring(1).Trim();
					string[] ArgSplit = A.Split(new char[] { '=' }, 2);

					// We accept -opt arguments (implicitly, these are
					// treated as -opt=YES type arguments) and explicit
					// -opt=val arguments.
					if ( ArgSplit.Length > 1 )
						ArgDict[ArgSplit[0].Trim().ToLower()] = ArgSplit[1].Trim();
					else
						ArgDict[ArgSplit[0].Trim().ToLower()] = true;
				}
				// Arguments beginning with "applaunch:" are assumed to be URL
				// arguments.  These can contain an entire set of command line
				// options within them, but everything internally will be
				// URL-encoded.
				else if ( Arg.Trim().ToUpper().StartsWith("APPLAUNCH:") )
				{
					// Remove the applaunch: beginning of the argument, and split
					// it into its subarguments.
					string[] ArgParts = Arg.Trim().Substring(10)
						.Trim().Split('&', '?');

					// URL's don't have a good facility to separate "bare" arguments
					// from -opt-type arguments, so we just have to force the first
					// part to be the "bare" url, while anything after the first splitter
					// will be the options.
					ArgDict[string.Empty] = HttpUtility.UrlDecode(ArgParts[0].Trim());
					string[] ArgFlags = new string[ArgParts.Length - 1];
					Array.Copy(ArgParts, 1, ArgFlags, 0, ArgFlags.Length);

					// Walk through each option.
					foreach ( string B in ArgFlags )
					{
						// Split this as above, on ='s.  We accept arguments
						// in the form of ?opt (implicitly ?opt=YES) and
						// explicitly ?opt=val
						string[] ArgSplit = B.Split(new char[] { '=' }, 2);
						if ( ArgSplit.Length > 1 )
							ArgDict[HttpUtility.UrlDecode(ArgSplit[0].Trim()).ToLower()]
								= HttpUtility.UrlDecode(ArgSplit[1].Trim());
						else
							ArgDict[HttpUtility.UrlDecode(ArgSplit[0].Trim()).ToLower()] = true;
					}
				}
				// Any "bare" arguments are saved as the URL to the object
				// to be launched.  Save the last found one in the empty-string
				// entry in the command line dictionary.
				else
					ArgDict[string.Empty] = Arg;
			}
			return ArgDict;
		}
		#endregion

		public static LaunchOptions FromCommandLine(string[] Args)
		{
			IDictionary ArgDict = ProcessArguments(Args);

			LaunchOptions LO = new LaunchOptions();

			foreach ( PropertyInfo PI in LO.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public) )
				if ( PI.CanWrite && (PI.GetIndexParameters().Length == 0) )
				{
					foreach ( DefaultValueAttribute DV in PI.GetCustomAttributes(
						typeof(DefaultValueAttribute), true) )
						if ( DV.Value.GetType() == PI.PropertyType )
							PI.SetValue(LO, DV.Value, null);
					foreach ( LaunchOptionNameAttribute LONA in PI.GetCustomAttributes(
						typeof(LaunchOptionNameAttribute), true) )
					{
						string ArgName = LONA.ArgName.Trim().ToLower();
						if ( ArgDict.Contains(ArgName) )
						{
							try
							{
								if ( PI.PropertyType == typeof(string) )
									PI.SetValue(LO, ArgDict[ArgName].ToString(), null);
								else if ( PI.PropertyType == typeof(bool) )
									PI.SetValue(LO, true, null);
								else if ( PI.PropertyType == typeof(int) )
									PI.SetValue(LO, Convert.ToInt32(ArgDict[ArgName].ToString()), null);
							}
							catch ( Exception Ex )
							{
								throw new Exception("An error occurred while attempting to "
									+ "parse command line option \"" + ArgName + "\": "
									+ Ex.Message, Ex);
							}
							ArgDict.Remove(ArgName);
						}
					}
				}

			foreach ( object O in ArgDict.Keys )
				throw new Exception("Unrecognized argument: " + O.ToString());

			return LO;
		}
	}
}

