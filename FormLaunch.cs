﻿using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace applaunchProtocolHandler
{
	public partial class FormLaunch : Form
	{
		protected LaunchOptions Options = null;
		protected Thread LaunchAppThread = null;

		public FormLaunch(LaunchOptions NewOptions)
		{
			Options = NewOptions;
			InitializeComponent();
		}

		private void FormLaunch_Load(object sender, EventArgs e)
		{
			Location = new Point(-Width * 2, -Height * 2);
		}

		private void timerTrayIcon_Tick(object sender, EventArgs e)
		{
			Icon I = this.notifyIconTray.Icon;
			Bitmap Q = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
			double Theta = ((TimeSpan)(DateTime.Now - new DateTime(1970, 1, 1))).TotalSeconds * 200;
			Theta -= Math.Truncate(Theta / 360) * 360;
			Graphics G = Graphics.FromImage(Q);
			G.Clear(SystemColors.Control);
			G.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
			G.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
			G.FillEllipse(Brushes.Black, 1, 1, 13, 13);
			G.DrawArc(new Pen(Brushes.Red, 3), 1, 1, 13, 13, (float)Theta, 90);
			G.DrawArc(new Pen(Brushes.Blue, 3), 1, 1, 14, 13, (float)Theta + 90, 90);
			G.DrawArc(new Pen(Brushes.Yellow, 3), 1, 1, 13, 13, (float)(Theta + 180), 90);
			G.DrawArc(new Pen(Brushes.Green, 3), 1, 1, 13, 13, (float)(Theta + 270), 90);
			G.DrawArc(new Pen(Brushes.Green, 3), 4, 4, 7, 7, (float)(-Theta), 90);
			G.DrawArc(new Pen(Brushes.Yellow, 3), 4, 4, 7, 7, (float)(-Theta + 90), 90);
			G.DrawArc(new Pen(Brushes.Blue, 3), 4, 4, 7, 7, (float)(-Theta + 180), 90);
			G.DrawArc(new Pen(Brushes.Red, 3), 4, 4, 7, 7, (float)(-Theta + 270), 90);
			this.notifyIconTray.Icon = Icon.FromHandle(Q.GetHicon());
			Q.Dispose();
			if ( I != null )
				I.Dispose();

			if ( LaunchAppThread == null )
			{
				LaunchAppThread = new Thread(new ThreadStart(LaunchAppThreadMain));
				LaunchAppThread.Start();
			}
			else if ( !LaunchAppThread.IsAlive )
			{
				this.notifyIconTray.Visible = false;
				this.Close();
			}
		}

		#region Public Static Method CacheDir
		/// <summary>
		/// Converts a real path name pointing to a local or network resource
		/// into its corresponding local cache name in the app's permanent
		/// cache.
		/// </summary>
		public static string CacheDir(string OutsidePath)
		{
			// Add the outside path to the cache directory.  Strip off any
			// drive-specs (replace colons with underscores) and strip off double
			// backslashes, so server names become just directories.
			string TempDir = Path.Combine(Path.GetTempPath(),
				Regex.Replace(Application.ProductName, "[^A-Za-z0-9]", ""));
			string CacheDir = OutsidePath.Replace("/", @"\");
			foreach ( char X in "*:?\"<>|".ToCharArray() )
				CacheDir = CacheDir.Replace(X.ToString(), "_");
			CacheDir = Regex.Replace(CacheDir, @"\\\\*", @"\");
			CacheDir = Regex.Replace(CacheDir, @"^\\*", "");

			return Path.Combine(TempDir, CacheDir);
		}
		#endregion

		#region Public Static Method CopyTree
		/// <summary>
		/// This recursively copies any directory tree.
		/// </summary>
		public static void CopyTree(string SrcDir, string DestDir, bool Recurse)
		{
			DirectoryInfo Src = new DirectoryInfo(SrcDir);

			// Make sure the destination directory exists.
			try
			{
				if ( !Directory.Exists(DestDir) )
					Directory.CreateDirectory(DestDir);
			}
			catch { }

			// Loop through all files in the source data,
			// checking each one and automatically copying
			// into the destination.
			foreach ( FileInfo FI in Src.GetFiles() )
			{
				string Dst = Path.Combine(DestDir, FI.Name);

				// Check if the destination file already exists and
				// has a matching file size and modtime.  If so, assume
				// it's already cached and don't re-copy it.
				bool DoCopy = true;
				if ( File.Exists(Dst) )
				{
					FileInfo DF = new FileInfo(Dst);
					if ( (DF.LastWriteTimeUtc == FI.LastWriteTimeUtc)
						&& (DF.Length == FI.Length) )
						DoCopy = false;
				}

				// Do our best to copy the file, if necessary.  Dodge
				// possible read-only attributes (and make sure to reset
				// them on the new files too, in case some temp or config
				// files are included in the transfer).
				if ( DoCopy )
				{
					try
					{
						if ( File.Exists(Dst) )
						{
							File.SetAttributes(Dst, FileAttributes.Normal);
							File.Delete(Dst);
						}
					}
					catch { }
					FI.CopyTo(Dst, true);
					try
					{
						File.SetAttributes(Dst, FileAttributes.Normal);
					}
					catch { }
				}
			}

			// Recursively copy any subdirectories.
			if ( Recurse )
				foreach ( DirectoryInfo DI in Src.GetDirectories() )
					CopyTree(DI.FullName, Path.Combine(DestDir, DI.Name), Recurse);
		}
		#endregion

		#region Method LaunchAppThreadMain
		/// <summary>
		/// Copies the app and all associated files to the local cache
		/// (if updates are necessary) then launches it from local cache.
		/// </summary>
		private void LaunchAppThreadMain()
		{
			// TODO: ABSTRACT THIS OUT SO IT CAN HANDLE URL TYPES OTHER
			// THAN FILE (I.E. LAUNCH FROM HTTP?)

			// Make sure the Exe file in question actually exists.
			try { if ( !File.Exists(Options.LaunchURL) ) throw new Exception(); }
			catch { AppMain.DisplayError("Path " + Options.LaunchURL + " was not found"); return; }

			// Pull the application onto the local machine.
			string CachePath = CacheDir(Options.LaunchURL);
			CopyTree(Path.GetDirectoryName(Options.LaunchURL), Path.GetDirectoryName(CachePath), true);

			// Set current working Dir to the executible's dir; this is necessary
			// for certain apps that don't sense "self" location.  Run the app.
			Directory.SetCurrentDirectory(Path.GetDirectoryName(CachePath));
			if ( (Options.CmdLineArgs != null) && (Options.CmdLineArgs.Length > 0) )
				Process.Start(CachePath, Options.CmdLineArgs);
			else
				Process.Start(CachePath);
		}
		#endregion

	}
}
