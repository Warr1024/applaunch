﻿namespace applaunchProtocolHandler
{
    partial class FormAsk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanelButtons = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonAllowOnce = new System.Windows.Forms.Button();
            this.buttonBlockOnce = new System.Windows.Forms.Button();
            this.buttonAllowAlways = new System.Windows.Forms.Button();
            this.buttonBlockAlways = new System.Windows.Forms.Button();
            this.labelURL = new System.Windows.Forms.Label();
            this.tableLayoutPanelOverall = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanelButtons.SuspendLayout();
            this.tableLayoutPanelOverall.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanelButtons
            // 
            this.flowLayoutPanelButtons.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanelButtons.AutoSize = true;
            this.flowLayoutPanelButtons.Controls.Add(this.buttonAllowOnce);
            this.flowLayoutPanelButtons.Controls.Add(this.buttonBlockOnce);
            this.flowLayoutPanelButtons.Controls.Add(this.buttonAllowAlways);
            this.flowLayoutPanelButtons.Controls.Add(this.buttonBlockAlways);
            this.flowLayoutPanelButtons.Location = new System.Drawing.Point(0, 27);
            this.flowLayoutPanelButtons.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanelButtons.Name = "flowLayoutPanelButtons";
            this.flowLayoutPanelButtons.Size = new System.Drawing.Size(334, 27);
            this.flowLayoutPanelButtons.TabIndex = 0;
            this.flowLayoutPanelButtons.WrapContents = false;
            // 
            // buttonAllowOnce
            // 
            this.buttonAllowOnce.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonAllowOnce.AutoSize = true;
            this.buttonAllowOnce.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAllowOnce.Location = new System.Drawing.Point(4, 2);
            this.buttonAllowOnce.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.buttonAllowOnce.Name = "buttonAllowOnce";
            this.buttonAllowOnce.Size = new System.Drawing.Size(71, 23);
            this.buttonAllowOnce.TabIndex = 0;
            this.buttonAllowOnce.Text = "Allow Once";
            this.buttonAllowOnce.UseVisualStyleBackColor = true;
            this.buttonAllowOnce.Click += new System.EventHandler(this.buttonAllowOnce_Click);
            // 
            // buttonBlockOnce
            // 
            this.buttonBlockOnce.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonBlockOnce.AutoSize = true;
            this.buttonBlockOnce.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonBlockOnce.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonBlockOnce.Location = new System.Drawing.Point(83, 2);
            this.buttonBlockOnce.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.buttonBlockOnce.Name = "buttonBlockOnce";
            this.buttonBlockOnce.Size = new System.Drawing.Size(73, 23);
            this.buttonBlockOnce.TabIndex = 1;
            this.buttonBlockOnce.Text = "Block Once";
            this.buttonBlockOnce.UseVisualStyleBackColor = true;
            this.buttonBlockOnce.Click += new System.EventHandler(this.buttonBlockOnce_Click);
            // 
            // buttonAllowAlways
            // 
            this.buttonAllowAlways.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonAllowAlways.AutoSize = true;
            this.buttonAllowAlways.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAllowAlways.Location = new System.Drawing.Point(164, 2);
            this.buttonAllowAlways.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.buttonAllowAlways.Name = "buttonAllowAlways";
            this.buttonAllowAlways.Size = new System.Drawing.Size(78, 23);
            this.buttonAllowAlways.TabIndex = 2;
            this.buttonAllowAlways.Text = "Always Allow";
            this.buttonAllowAlways.UseVisualStyleBackColor = true;
            this.buttonAllowAlways.Click += new System.EventHandler(this.buttonAllowAlways_Click);
            // 
            // buttonBlockAlways
            // 
            this.buttonBlockAlways.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonBlockAlways.AutoSize = true;
            this.buttonBlockAlways.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonBlockAlways.Location = new System.Drawing.Point(250, 2);
            this.buttonBlockAlways.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.buttonBlockAlways.Name = "buttonBlockAlways";
            this.buttonBlockAlways.Size = new System.Drawing.Size(80, 23);
            this.buttonBlockAlways.TabIndex = 3;
            this.buttonBlockAlways.Text = "Always Block";
            this.buttonBlockAlways.UseVisualStyleBackColor = true;
            this.buttonBlockAlways.Click += new System.EventHandler(this.buttonBlockAlways_Click);
            // 
            // labelURL
            // 
            this.labelURL.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelURL.AutoSize = true;
            this.labelURL.Location = new System.Drawing.Point(137, 7);
            this.labelURL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelURL.Name = "labelURL";
            this.labelURL.Size = new System.Drawing.Size(60, 13);
            this.labelURL.TabIndex = 0;
            this.labelURL.Text = "applaunch:";
            // 
            // tableLayoutPanelOverall
            // 
            this.tableLayoutPanelOverall.AutoSize = true;
            this.tableLayoutPanelOverall.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanelOverall.ColumnCount = 1;
            this.tableLayoutPanelOverall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelOverall.Controls.Add(this.labelURL, 0, 0);
            this.tableLayoutPanelOverall.Controls.Add(this.flowLayoutPanelButtons, 0, 1);
            this.tableLayoutPanelOverall.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanelOverall.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanelOverall.Name = "tableLayoutPanelOverall";
            this.tableLayoutPanelOverall.RowCount = 2;
            this.tableLayoutPanelOverall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelOverall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelOverall.Size = new System.Drawing.Size(334, 54);
            this.tableLayoutPanelOverall.TabIndex = 0;
            // 
            // FormAsk
            // 
            this.AcceptButton = this.buttonAllowOnce;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.buttonBlockOnce;
            this.ClientSize = new System.Drawing.Size(539, 128);
            this.Controls.Add(this.tableLayoutPanelOverall);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAsk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormAsk_Load);
            this.flowLayoutPanelButtons.ResumeLayout(false);
            this.flowLayoutPanelButtons.PerformLayout();
            this.tableLayoutPanelOverall.ResumeLayout(false);
            this.tableLayoutPanelOverall.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelButtons;
        private System.Windows.Forms.Button buttonAllowAlways;
        private System.Windows.Forms.Button buttonAllowOnce;
        private System.Windows.Forms.Button buttonBlockOnce;
        private System.Windows.Forms.Button buttonBlockAlways;
        private System.Windows.Forms.Label labelURL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelOverall;
    }
}