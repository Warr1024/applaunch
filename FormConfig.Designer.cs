﻿namespace applaunchProtocolHandler
{
	partial class FormConfig
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if ( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfig));
			this.labelLicense2 = new System.Windows.Forms.Label();
			this.groupBoxRules = new System.Windows.Forms.GroupBox();
			this.labelNoRules = new System.Windows.Forms.Label();
			this.listViewRules = new System.Windows.Forms.ListView();
			this.columnHeaderAction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeaderApp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.labelLicense1 = new System.Windows.Forms.Label();
			this.labelLicense3 = new System.Windows.Forms.Label();
			this.tableLayoutPanelOverall = new System.Windows.Forms.TableLayoutPanel();
			this.buttonRemoveRule = new System.Windows.Forms.Button();
			this.buttonClose = new System.Windows.Forms.Button();
			this.groupBoxRules.SuspendLayout();
			this.tableLayoutPanelOverall.SuspendLayout();
			this.SuspendLayout();
			// 
			// labelLicense2
			// 
			this.labelLicense2.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.labelLicense2.AutoSize = true;
			this.tableLayoutPanelOverall.SetColumnSpan(this.labelLicense2, 2);
			this.labelLicense2.Location = new System.Drawing.Point(4, 25);
			this.labelLicense2.Margin = new System.Windows.Forms.Padding(4);
			this.labelLicense2.Name = "labelLicense2";
			this.labelLicense2.Size = new System.Drawing.Size(536, 26);
			this.labelLicense2.TabIndex = 1;
			this.labelLicense2.Text = resources.GetString("labelLicense2.Text");
			// 
			// groupBoxRules
			// 
			this.tableLayoutPanelOverall.SetColumnSpan(this.groupBoxRules, 2);
			this.groupBoxRules.Controls.Add(this.labelNoRules);
			this.groupBoxRules.Controls.Add(this.listViewRules);
			this.groupBoxRules.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxRules.Location = new System.Drawing.Point(0, 141);
			this.groupBoxRules.Margin = new System.Windows.Forms.Padding(0);
			this.groupBoxRules.Name = "groupBoxRules";
			this.groupBoxRules.Padding = new System.Windows.Forms.Padding(4);
			this.groupBoxRules.Size = new System.Drawing.Size(580, 187);
			this.groupBoxRules.TabIndex = 3;
			this.groupBoxRules.TabStop = false;
			this.groupBoxRules.Text = "Rules";
			// 
			// labelNoRules
			// 
			this.labelNoRules.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelNoRules.Location = new System.Drawing.Point(4, 18);
			this.labelNoRules.Name = "labelNoRules";
			this.labelNoRules.Size = new System.Drawing.Size(572, 165);
			this.labelNoRules.TabIndex = 1;
			this.labelNoRules.Text = resources.GetString("labelNoRules.Text");
			this.labelNoRules.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// listViewRules
			// 
			this.listViewRules.Activation = System.Windows.Forms.ItemActivation.OneClick;
			this.listViewRules.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderAction,
            this.columnHeaderApp});
			this.listViewRules.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listViewRules.FullRowSelect = true;
			this.listViewRules.GridLines = true;
			this.listViewRules.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.listViewRules.HideSelection = false;
			this.listViewRules.LabelWrap = false;
			this.listViewRules.Location = new System.Drawing.Point(4, 18);
			this.listViewRules.Margin = new System.Windows.Forms.Padding(2, 0, 2, 4);
			this.listViewRules.Name = "listViewRules";
			this.listViewRules.ShowItemToolTips = true;
			this.listViewRules.Size = new System.Drawing.Size(572, 165);
			this.listViewRules.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.listViewRules.TabIndex = 0;
			this.listViewRules.UseCompatibleStateImageBehavior = false;
			this.listViewRules.View = System.Windows.Forms.View.Details;
			// 
			// columnHeaderAction
			// 
			this.columnHeaderAction.Text = "Action";
			// 
			// columnHeaderApp
			// 
			this.columnHeaderApp.Text = "Launch URL";
			// 
			// labelLicense1
			// 
			this.labelLicense1.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.labelLicense1.AutoSize = true;
			this.tableLayoutPanelOverall.SetColumnSpan(this.labelLicense1, 2);
			this.labelLicense1.Location = new System.Drawing.Point(4, 4);
			this.labelLicense1.Margin = new System.Windows.Forms.Padding(4);
			this.labelLicense1.Name = "labelLicense1";
			this.labelLicense1.Size = new System.Drawing.Size(320, 13);
			this.labelLicense1.TabIndex = 0;
			this.labelLicense1.Text = "Copyright (C)2007-2009 by Aaron Suen <warr1024@gmail.com>";
			// 
			// labelLicense3
			// 
			this.labelLicense3.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.labelLicense3.AutoSize = true;
			this.tableLayoutPanelOverall.SetColumnSpan(this.labelLicense3, 2);
			this.labelLicense3.Location = new System.Drawing.Point(4, 59);
			this.labelLicense3.Margin = new System.Windows.Forms.Padding(4);
			this.labelLicense3.Name = "labelLicense3";
			this.labelLicense3.Size = new System.Drawing.Size(563, 78);
			this.labelLicense3.TabIndex = 2;
			this.labelLicense3.Text = resources.GetString("labelLicense3.Text");
			// 
			// tableLayoutPanelOverall
			// 
			this.tableLayoutPanelOverall.ColumnCount = 2;
			this.tableLayoutPanelOverall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanelOverall.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanelOverall.Controls.Add(this.buttonRemoveRule, 0, 4);
			this.tableLayoutPanelOverall.Controls.Add(this.labelLicense3, 0, 2);
			this.tableLayoutPanelOverall.Controls.Add(this.labelLicense2, 0, 1);
			this.tableLayoutPanelOverall.Controls.Add(this.labelLicense1, 0, 0);
			this.tableLayoutPanelOverall.Controls.Add(this.groupBoxRules, 0, 3);
			this.tableLayoutPanelOverall.Controls.Add(this.buttonClose, 1, 4);
			this.tableLayoutPanelOverall.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanelOverall.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanelOverall.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanelOverall.Name = "tableLayoutPanelOverall";
			this.tableLayoutPanelOverall.RowCount = 5;
			this.tableLayoutPanelOverall.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanelOverall.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanelOverall.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanelOverall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanelOverall.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanelOverall.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanelOverall.Size = new System.Drawing.Size(580, 357);
			this.tableLayoutPanelOverall.TabIndex = 0;
			// 
			// buttonRemoveRule
			// 
			this.buttonRemoveRule.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.buttonRemoveRule.AutoSize = true;
			this.buttonRemoveRule.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.buttonRemoveRule.Location = new System.Drawing.Point(3, 331);
			this.buttonRemoveRule.Name = "buttonRemoveRule";
			this.buttonRemoveRule.Size = new System.Drawing.Size(137, 23);
			this.buttonRemoveRule.TabIndex = 6;
			this.buttonRemoveRule.Text = "Remove Selected Rule(s)";
			this.buttonRemoveRule.UseVisualStyleBackColor = true;
			this.buttonRemoveRule.Click += new System.EventHandler(this.buttonRemoveRule_Click);
			// 
			// buttonClose
			// 
			this.buttonClose.AutoSize = true;
			this.buttonClose.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonClose.Location = new System.Drawing.Point(534, 331);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(43, 23);
			this.buttonClose.TabIndex = 5;
			this.buttonClose.Text = "Close";
			this.buttonClose.UseVisualStyleBackColor = true;
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// FormConfig
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.buttonClose;
			this.ClientSize = new System.Drawing.Size(580, 357);
			this.Controls.Add(this.tableLayoutPanelOverall);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MinimumSize = new System.Drawing.Size(588, 384);
			this.Name = "FormConfig";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.FormConfig_Load);
			this.groupBoxRules.ResumeLayout(false);
			this.tableLayoutPanelOverall.ResumeLayout(false);
			this.tableLayoutPanelOverall.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label labelLicense2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelOverall;
		private System.Windows.Forms.Label labelLicense3;
		private System.Windows.Forms.Label labelLicense1;
		private System.Windows.Forms.GroupBox groupBoxRules;
		private System.Windows.Forms.ListView listViewRules;
		private System.Windows.Forms.ColumnHeader columnHeaderAction;
		private System.Windows.Forms.ColumnHeader columnHeaderApp;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Label labelNoRules;
		private System.Windows.Forms.Button buttonRemoveRule;
	}
}